import axios from 'axios'
import React from 'react'

const HitApi = () => {
    let ReadContact = async()=>{ //async le hit handey vanera instruct garxa
        let obj ={
            url: "https://fake-api-nkzv.onrender.com/api/v1/contacts",
            method : "get"
        }
        let result =  await axios(obj) // await le kurera basxa hit nahaney samma 
        // console.log("hello")
       // console.log(result) // await ra async bina call garyo promises aauxa i.e. kaam hudai xa data paxi aauxa vaneko 
        console.log(result.data.data.results)
    }

    let createContact = async ()=>{
        let obj1 ={
            url: "https://fake-api-nkzv.onrender.com/api/v1/contacts",
            method: "POST",
            data:{
                fullName:"nitina thapa",
                address:"gagalphedi",
                phoneNumber:9849468999,
                email:"nitanthapa425@gmail.com"
                }
        }
        let result1 = await axios (obj1)
        console.log(result1)
    }
  return (
    <>
        <button onClick={()=>{
            ReadContact()
        }}>click button to read contact</button>

        <button onClick={()=>{
            createContact()
        }}>click button to create contact</button>
    </>
  )
}

export default HitApi
//axios lai jaile try catch rakhna paryo kina vane server down vako tha hudaina ani error handling ko lagi use garne
// same line ma code execute huna lai await async rakhne 
