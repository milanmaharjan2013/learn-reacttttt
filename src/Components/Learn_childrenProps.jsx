import React, { Children } from 'react'

const Learn_childrenProps = ({name,age,children}) => {
  return (
    <div>
      {name} <br />
      {age} <br />
      {children} 
      <p className='props'>this is children props to learn css</p>
    </div>
  )
}

export default Learn_childrenProps

//by default children hunxa