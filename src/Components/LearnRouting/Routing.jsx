import React from 'react'
import { Route, Routes } from 'react-router-dom'
import Form1 from '../Learn_Form/Form1'
import Form2 from '../Learn_Form/Form2'
import Counter from '../Counter'
import DynamicRoute from './DynamicRoute'
import SearchParameter from './SearchParameter'

const Routing = () => {
  return (
    <div>
        <Routes>
            <Route path='/' element={<div>This is home page</div>}></Route>
            <Route path='/about' element={<div>This is about us</div>}></Route>
            <Route path='/contact'element={<Form1></Form1>}></Route>
            <Route path='/form' element={<Form2></Form2>}></Route>
            <Route path='/counter' element={<Counter></Counter>}></Route>
            <Route path='/counter/:id' element={<div>THis is counter</div>}></Route>
            <Route path='/counter/:id/id1/:id2' element={<div>Learn to get dynamic route</div>}></Route>
            <Route path='/dynamicRoute/:id1/id2/:id3' element={<DynamicRoute></DynamicRoute>}></Route>
            <Route path='/searchParam' element={<SearchParameter></SearchParameter>}></Route>
        </Routes>
    </div>
  )
}

export default Routing

