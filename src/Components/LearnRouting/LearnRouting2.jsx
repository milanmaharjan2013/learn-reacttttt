import React from 'react'
import NavLinks from './NavLinks'
import Routing from './Routing'
import Navigate from './Navigate'

const LearnRouting2 = () => {
  return (
    <div>
        <NavLinks></NavLinks>
        <Routing></Routing>
        <Navigate></Navigate>
    </div>
  )
}

export default LearnRouting2