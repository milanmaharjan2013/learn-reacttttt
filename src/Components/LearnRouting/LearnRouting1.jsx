import React from 'react'
import { NavLink, Route, Routes } from 'react-router-dom'
import Form2 from '../Learn_Form/Form2'
import Select from '../Learn_Form/Select'
import ReadAllContacts from '../Learn_Form/ReadAllContacts'
import Form7 from '../Learn_Form/Form7'
import Counter from '../Counter'

const LearnRouting1 = () => {
  return (
    <div>Learn Routing 
        <br />
        <br />
        <NavLink to ="/" style={{marginLeft:"10px"}}>Home</NavLink>
        <NavLink to ="/about" style={{marginLeft:"10px"}}>About us</NavLink>
        <NavLink to ="/contact" style={{marginLeft:"10px"}}>Contact us</NavLink>
        <NavLink to ="/form" style={{marginLeft:"10px"}}>Fill the form</NavLink>
        <NavLink to ="/counter" style={{marginLeft:"10px"}}>Counter</NavLink>

        <Routes>
            <Route path='/' element={<Form2></Form2>}></Route>
            <Route path='/about' element={<Select></Select>}></Route>
            <Route path='/contact' element={<ReadAllContacts></ReadAllContacts>}></Route>
            <Route path='/form' element={<Form7></Form7>}></Route>
            <Route path='/counter' element={<Counter></Counter>}></Route>
        </Routes>

    </div>
  )
}

export default LearnRouting1
