import React from 'react'
import { useNavigate } from 'react-router-dom'

const Navigate = () => {
    let navigate = useNavigate()
  return (
    <div>
        <button onClick={()=>{
            // navigate("/contact")
            navigate("/contact",{replace: true})
        }}>go back to contact</button>
    </div>
  )
}

export default Navigate