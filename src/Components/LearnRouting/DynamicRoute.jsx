import React from 'react'
import { useParams } from 'react-router-dom'

const DynamicRoute = () => {
    let params = useParams()
    console.log(params)
  return (
    <div>
        Learn to get dynamic route
    </div>
  )
}

export default DynamicRoute