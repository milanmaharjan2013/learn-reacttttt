import React from 'react'
import { NavLink, Route, Routes } from 'react-router-dom'
import Form1 from '../Learn_Form/Form1'

const LearnRouting = () => {
  return (
    <div>
        {/* <a href="/A1">About</a>
        <br />
        aaaaaa
        <br /> */}
        {/* / => means localhost:3000 */}
        {/* /about => means localhost:3000/about */}
        {/* /a/a1 => means localhost:3000/a/a1 */}
       <NavLink to = "/"style={{marginLeft:"20px"}}>Home</NavLink>
       <NavLink to = "about"style={{marginLeft:"20px"}}>About</NavLink>
       <NavLink to = "contact"style={{marginLeft:"20px"}}>Contact Us</NavLink>
       <NavLink to = "form"style={{marginLeft:"20px"}}>Fill the form</NavLink>



       {/* Defining components for specific path */}
       <Routes>
        <Route path='/'element ={<div>This is home page</div>}></Route>
        <Route path='/about'element ={<div>This is about page</div>}></Route>
        <Route path='/contact'element ={<div>This is contact page</div>}></Route>
        <Route path='/form'element={<div><Form1></Form1></div>}></Route>
        <Route path="*" element={<div>404 Page</div>}></Route> 
        {/* 404 page execute, if the route is differ than other define route i.e 4 ota route bahek aru route aayo vane (*) ma janxa */}
        <Route path='/a' element={<div>a page</div>}></Route>
        <Route path='/a/a1'element={<div>a1 page</div>}></Route>
        <Route path='/a/a1/a2'element={<div>a2 page</div>}></Route>
        <Route path='/a/:any'element={<div>any page</div>}></Route> 
        {/* {/* : rakhyo vane route ho tara dyanamic parameter hunxa */
            //  : paxi id haleko ramro
        } 
       </Routes>
    </div>
  )
}

export default LearnRouting 
//NavLink euta component ma route aarko component ma 
//querey param ko matlab hudaina 

