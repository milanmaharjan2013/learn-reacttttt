import React from 'react'
import { useSearchParams } from 'react-router-dom'

const SearchParameter = () => {
let [searchParameters] = useSearchParams()
let name = searchParameters.get("name")
let age  = searchParameters.get("age")
  return (
    <div>SearchParameter
        <br />
        name is {name} <br />
        age is {age} <br />
    </div>
  )
}

export default SearchParameter