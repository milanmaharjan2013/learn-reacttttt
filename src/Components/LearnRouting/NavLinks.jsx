import React from 'react'
import { NavLink } from 'react-router-dom'

const NavLinks = () => {
  return (
    <div>
        <NavLink to ="/" style={{marginLeft:"10px"}}>Home</NavLink>
        <NavLink to ="/about" style={{marginLeft:"10px"}}>About us</NavLink>
        <NavLink to ="/contact" style={{marginLeft:"10px"}}>Contact us</NavLink>
        <NavLink to ="/form" style={{marginLeft:"10px"}}>Fill the form</NavLink>
        <NavLink to ="/counter" style={{marginLeft:"10px"}}>Counter</NavLink>
        {/* <NavLink to ="/dynamicRoute/:id1/id2/:id3"style={{marginLeft:"10px"}}>Tap to get dynamic route</NavLink> */}
    </div>
  )
}

export default NavLinks