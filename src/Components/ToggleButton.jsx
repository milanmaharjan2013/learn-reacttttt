import React, { useState } from 'react'

const ToggleButton = () => {
    let [show,setShow]=useState(false)
  return (
    <div>
        <br />
        {
            show? <p>Error has occured</p>:null
        }
        <button onClick={()=>{
            setShow(!show)
        }}>Toggle</button>
        <hr />
        <br />
    </div>
  )
}

export default ToggleButton