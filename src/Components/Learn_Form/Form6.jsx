// make a form with field
// 	productName
// 	productManagerEmail,
// 	staffEmail,
// 	password,
// 	gender,
// 	productManufacturerDate,
// 	ManagerIsMarried,
// 	ManagerSpouse,
// 	productLocation,
// 	productDescription,
// 	productIsAvailable
// 	at date use type = date and other things are same

import React, { useState } from 'react'

const Form6 = () => {
  let [productName, setProductName]=useState("")
  let [productManagerEmail, setProductManagerEmail]=useState("")
  let [staffEmail, setStaffEmail]=useState("")
  let[password, setPassword]=useState("")
  let[gender, setGender]=useState("")
  let[productManufacturerDate, setProductManufacturerDate]=useState("")
  let[isMarried,setIsMarried]=useState("")
  let[spouse,setSpouse]=useState("")
  let[productLocation,setProductLocation]=useState("")
  let [isProductAvailable,setIsProductAvailable]=useState("")
  let[productDescription,setProductDescription]=useState("")
  return (
  
    <>
        <form action="" onSubmit={(e)=>{
          e.preventDefault();
          let obj={
            productName,
            productManagerEmail,
            staffEmail,
            password,
            gender,
            productManufacturerDate,
            isMarried,
            spouse,
            productLocation,
            isProductAvailable,
            productDescription,
          }
          console.log(obj)
        }}>
          <label htmlFor="productName">Product Name: </label>
          <input type="text" id='productName' 
          value={productName} onChange={(e)=>{
            setProductName(e.target.value);
          }}
          /> <br />
          <label htmlFor="productManagerEmail">Product Manager Email: </label>
          <input type="email" name="" id="productManagerEmail"
          value={productManagerEmail} onChange={(e)=>{setProductManagerEmail(e.target.value);}}
          /> <br />
          <label htmlFor="staffEmail">Staff Email: </label>
          <input type="email" name="" id="staffEmail" 
          value={staffEmail} onChange={(e)=>{setStaffEmail(e.target.value)}}
          /> <br />
          <label htmlFor="password">Password: </label>
          <input type="password" id='password'  value={password}
          onChange={(e)=>{
            setPassword(e.target.value)
          }}
          /> <br />
          <label htmlFor="gender">Gender: </label>
        <label htmlFor="male" >Male</label>
        <input type="radio" id='male' value="male" checked={gender==="male"} onChange={(e)=>{setGender(e.target.value)}}/>
        <label htmlFor="female">Female</label>
        <input type="radio" id='female' value="female" checked={gender==="female"} onChange={(e)=>{setGender(e.target.value)}}/>
        <label htmlFor="others">Other</label>
        <input type="radio" id=' others' value="others" checked={gender==="others"} onChange={(e)=>{setGender(e.target.value)}} /> <br />
        <label htmlFor="productManufacturerDate">Product Manufacturer Date: </label>
        <input type="date" id='productManufacturerDate'
        value={productManufacturerDate} onChange={(e)=>{setProductManufacturerDate(e.target.value)}}
        /><br />
        <label htmlFor="isMarried">Is Married:</label>
        <input type="checkbox" name="" id="isMarried" 
        checked={isMarried} onChange={(e)=>{setIsMarried(e.target.checked)}} 
        /><br />
        <label htmlFor="spouse">Manager Spouse: </label>
        <label htmlFor="husband">Husband</label>
        <input type="radio" id="husband" value="husband" checked={spouse==="husband"} onChange={(e)=>{
          setSpouse(e.target.value)
        }} />
        <label htmlFor="wife">Wife</label>
        <input type="radio" id='wife'
        value="wife" checked={spouse==="wife"} onChange={(e)=>{
          setSpouse(e.target.value)
        }} /><br />
        <label htmlFor="productLocation">Product Location: </label>
        <input type="text" name="" id="productLocation" value={productLocation} onChange={(e)=>{
          setProductLocation(e.target.value)
        }} /> <br />
        <label htmlFor="isProductAvailable">Is Product Available? </label>
        <input type="checkbox" id='isProductAvailable' checked={isProductAvailable} 
        onChange={(e)=>{
          setIsProductAvailable(e.target.checked)
        }}
        /> <br />
        <label htmlFor="productDescription">Product Description: </label>
        <textarea name="" id="productDescription" value={productDescription} onChange={(e)=>{
          setProductDescription(e.target.value)
        }} ></textarea> <br />
        <button type='submit'>Submit</button>
        </form>
    </>
  )
}

export default Form6