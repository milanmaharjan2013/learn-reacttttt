// "fullName":"nitan",
//     "address":"gagalphedi",
//     "phoneNumber":9849468999,
//     "email":"nitanthapa425@gmail.com"


import axios from 'axios'
import React, { useState } from 'react'
import { baseUrl } from '../../Config/config'

const Form7 = () => {
    let [fullName, setFullName] =useState("nitan")
    let [address, setAddress] = useState("gagalphedi")
    let[phoneNumber, setPhoneNumber] = useState("9849468999")
    let[email, setEmail] = useState("nitanthapa425@gmail.com")
  return (
    <>
        <form action="" onSubmit={async(e)=>{
            e.preventDefault();
            let data= {
                fullName,
                address,
                phoneNumber,
                email
            }
            let info={
                url:`${baseUrl}/contacts`,
                method :"post",
                data
            }
            // let result = await axios(info)
            // console.log(data)
            // console.log(result)  
            // console.log(result.data.message34)

            try {
            let result = await axios(info)
            console.log(result.data.message)
            } catch (error) {
                console.log(error.message)
            }
        }}>
            <label htmlFor="fullName">Full Name: </label>
            <input type="text" id='fullName' value={fullName} onChange={(e)=>{
                setFullName(e.target.value)
            }} /> <br />
            <label htmlFor="address">Address: </label>
            <input type="text" id='address' value={address} onChange={(e)=>{
                setAddress(e.target.value)
            }} /> <br />
            <label htmlFor="phoneNumber">Phone Number: </label>
            <input type="number" name="" id="phoneNumber" value={phoneNumber} onChange={(e)=>{
                setPhoneNumber(e.target.value)
            }}/> <br />
            <label htmlFor="email">Email: </label>
            <input type="email" name="" id="email"  value={email} onChange={(e)=>{
                setEmail(e.target.value)
            }}/> <br />
            <button type='submit'>Submit</button>
        </form>
    </>
  )
}

export default Form7