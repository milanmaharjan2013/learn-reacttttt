import React, { useState } from 'react'

const Select = () => {
    let[movies, setMovies]=useState("")
      let favMovies = [
        {label:"Select Movie", value: "", disabled: true},
        {label:"3 idiots", value:"3 idiots"},
        {label:"Tere Naam", value:"tere naam"},
        {label:"Pushpa",value:"pushpa"},
        {label:"Singham", value:"singham"},
        {label:"Koyla",value:"koyla"}
      ]
      let[gender,setGender]=useState("")
      let genders = [
        { label: "Male", value: "male" },
        { label: "Female", value: "female" },
        { label: "Other", value: "other" },
      ];
  return (
    <>
        <form action="" onSubmit={(e)=>{
            e.preventDefault();
            let obj = {
                movie: movies
            }
            console.log(obj)
            let arr = [movies]
            console.log(`My favorite movie is ${movies}`)
        }}>
            <label htmlFor="movie">Select Movies:</label>
     <select name="" id="movie" value={movies} onChange={(e)=>{
        setMovies(e.target.value)
     }}>
        {
            favMovies.map((value,ine)=>{
                return (
                    <option key={ine} value={value.value} disabled={value.disabled}>{value.label}</option>
                )
            })
        }
     </select>
     <br />
     <br />
        <label htmlFor="">Gender</label>
        {
            genders.map((value,i)=>{
                return (
                    <>
                        <label htmlFor={value.value}>{value.label}</label>
                        <input type="text" />
                    </>
                )
            })
        }
     <button type='submit'>Submit</button>
        </form>
        </>
  )
}

export default Select


// <label htmlFor="male">Gender </label>
//       {genders.map((item, i) => {
//         return (
//           <>
//             <label htmlFor={item.value}>{item.label} </label>
//             <input
//               checked={gender === item.value}
//               onChange={(e) => {
//                 setGender(e.target.value);
//               }}
//               type="radio"
//               id={item.value}
//               value={item.value}
//             ></input>
//           </>
//         );
//       })}