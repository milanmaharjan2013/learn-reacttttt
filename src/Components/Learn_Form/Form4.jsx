import React, { useState } from 'react'

const Form4 = () => {
    let [firstName, set_firstName]=useState("")
    let [lastName, set_lastName]=useState("")
    let [address, set_address]=useState("")
    let [phoneNumber, set_phoneNumber]=useState("")
    let [email, set_email]=useState("")
    let [password, set_password]=useState("")
  return (
    <>
       <div style={{textAlign:"center", margin:"50px", }}>
        <form action="" onSubmit={(e)=>{
            e.preventDefault();
            let obj= {
                First_Name :firstName,
                Last_Name : lastName,
                Address :address,
                Phone_Number :phoneNumber,
                Email_Address :email,
                Password : password
            }
            console.log(obj)
        }}>
            <label htmlFor="firstName">First Name: </label>
            <input type="text" id='firstName' placeholder='Enter Your First Name' 
            value={firstName} onChange={(e)=>{set_firstName(e.target.value)}}/> <br />
            <label htmlFor="lastName">Last Name: </label>
            <input type="text" id='lastName' placeholder='Enter Your Last Name' 
            value ={lastName} onChange={(e)=>{set_lastName(e.target.value)}} /> <br />
            <label htmlFor="address">Address: </label>
            <input type="text" id='address' placeholder='Enter Your Address' 
            value={address} onChange={(e)=>{set_address(e.target.value)}}/><br></br>
            <label htmlFor="number">Phone Number: </label>
            <input type="number" name="" id="number" placeholder='Enter Your Phone Number'
            value={phoneNumber} onChange={(e)=>{set_phoneNumber(e.target.value)}}
            /> <br></br>
            <label htmlFor="email">Email Address: </label>
            <input type="email" id='email' placeholder='Enter Your Email Address' value={email} onChange={(e)=>{set_email(e.target.value)}} /> <br></br>
            <label htmlFor="password">Password: </label>
            <input type="password"id='password' placeholder='Enter Your Password' 
            value={password} onChange={(e)=>{set_password(e.target.value)}}/> <br></br>
            <button>Submit</button>
        </form>
       </div>
    </>
  )
}

export default Form4