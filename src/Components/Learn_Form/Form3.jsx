import React, { useState } from 'react'

const Form3 = () => {
    let [firstName, set_firstName]=useState('')
    let [lastName, set_lastName]=useState('')
    let [address, setAddress]=useState('')
    let [email, setEmail]=useState('')
    let [password, setPassword]=useState('')
    let [number, setNumber]=useState('')
  return (
    <>
        <form action="" onSubmit={(e)=>{
            e.preventDefault()
            let obj ={
                firstName : firstName,
                lastName : lastName,
                address : address,
                email : email,
                password : password,
                number : number
            }
            console.log(obj)
            
        }}>
            <label htmlFor="firstName">First Name: </label>
            <input type="text" id='firstName' 
            value ={firstName} 
            onChange={(e)=>{
                set_firstName(e.target.value)
            }}
            /> <br />
            <label htmlFor="lastName">Last Name: </label>
            <input type="text" id='lastName' value={lastName} onChange={(e)=>{
                set_lastName(e.target.value)
            }} /> <br />
            <label htmlFor="address">Address: </label>
            <input type="text" name="" id="address"  value={address} onChange={(e)=>{
                setAddress(e.target.value)
            }}/> <br />
            <label htmlFor="email">Email Address:</label>
            <input type="email" id='email' value={email} onChange={(e)=>{
                setEmail(e.target.value)
            }} /> <br />
            <label htmlFor="password">Password:</label>
            <input type="password" id='password'value={password} onChange={(e)=>{
                setPassword(e.target.value)
            }} /> <br />
            <label htmlFor="number">Mobile Number: </label>
            <input type="number" id='number' value={number} onChange={(e)=>{
                setNumber(e.target.value)
            }} />
            <br />
            <button type='submit'>Submit</button>
        </form>
        
    </>
  )
}

export default Form3