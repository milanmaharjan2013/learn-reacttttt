import React, { useEffect, useState } from 'react'
import { baseUrl } from '../../Config/config'
import axios from 'axios'

const ReadAllContacts = () => {    
    let [contacts, setContacts]=useState([])
    let readAllContacts =async()=>{
        let info = {
            url :`${baseUrl}/contacts`,
            method:"get"
        }
        let result= await axios(info)
        setContacts(result.data.data.results)
        // console.log(result)
        // console.log(result.data.data.results)
    }
   useEffect(()=>{
    readAllContacts()
   },[])
   let deleteContacts = async(_id)=>{
    let info ={
        url:`${baseUrl}/contacts/${_id}`,
        method: "delete"
    }
    let result  = await axios(info)
   }
  return (
    <div style={{border:"1px solid red"}}>read all contacts
        <div> <br />
            {
                contacts.map((item,i)=>{
                    return(
                        <div key={i} style={{border:"1px solid black"}}>
                            <p>Full Name is: {item.fullName}</p>
                            <p>Address is: {item.address}</p>
                            <p>Email is: {item.email}</p>
                            <p>Phone Number is: {item.phoneNumber}</p>
                            <button onClick={async()=>{
                                await deleteContacts(item._id)
                                await readAllContacts()
                            }}>Delete</button>
                        </div>
                    )
                })
            }
        </div>
    </div>
  )
}

export default ReadAllContacts
//deleteall garda read all api invalidate garna parxa
// update all garda pani 