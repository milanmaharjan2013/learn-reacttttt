import React, { useState } from 'react'

const Form2 = () => {
    let[name, setName]=useState('')
    let [address, setAddress]=useState('')
    let [password, setPassword]=useState('')
    let [description, setDescription]=useState('')
    let [isMarried, setIsMarried]=useState(false)
    let [country, setCountry]=useState("")
    let countries = [
        { label: "Select Country", value: "", disabled: true },
        { label: "Nepal", value: "nepal" },
        { label: "China", value: "china" },
        { label: "India", value: "india" },
        { label: "America", value: "america" },
      ]
      let[gender,setGender]=useState("male")
      
  return (
    <form action="" onSubmit={(e)=>{
        e.preventDefault()
        let obj = {
            name: name,
            address: address,
            password: password,
            feedbacks: description,
            isMarried: isMarried,
            // country:country
            country, //key ra value same cha vane euta matrai lekhey ni hunxa
            gender
        }
        console.log(obj)
    }}>
        <label htmlFor="name">Name: </label>
        <input id='name' type="text" placeholder='Name' value = {name}
         onChange={(e)=>{setName(e.target.value)}} /><br />
        <label htmlFor="address">Address:</label>
        <input id='address' type="text" placeholder='Address' value ={address} onChange={(e)=>{
            setAddress(e.target.value)
        }}/><br />
        <label htmlFor="password">Password:</label>
        <input type="password" name="" id="password" placeholder='password'value={password} onChange={(e)=>{
            setPassword(e.target.value)
        }} /> <br />
        <label htmlFor="description"></label>
        <textarea name="" id="description" placeholder='Feedbacks'value={description} onChange={(e)=>{
            setDescription(e.target.value)
        }}
        // rows = {30}
        // cols={50}
         /> <br />
    <label htmlFor="isMarried">isMarried:</label>
    <input type="checkbox" id='isMarried' checked={isMarried} onChange={(e)=>{
        setIsMarried(e.target.checked)
    }}/> <br />
    {/* <label htmlFor="countries" >Select Country:</label>
    <select name="" id="countries" value={country} onChange={(e)=>{setCountry(e.target.value)}}>
        <option value="" disabled={true}>Select Country</option>
        <option value="nepal">Nepal</option>
        <option value="india">India</option>
        <option value="america">America</option>
        <option value="australia">Australia</option>
        <option value="japan">Japan</option>
        <option value="korea">Korea</option>
        <option value="china">China</option>
    </select> */}

    

    <label htmlFor="countries">Select Country</label>
    <select name="" id="countries" value={country} onChange={(e)=>{setCountry(e.target.value)}}>
    {
        countries.map((item,i)=>{
            //label = select country value ="" disabled : true
            return(
                <option key={i}value={item.value} disabled={item.disabled}>{item.label}</option>
            )
        })
     }
        
    </select>
 <br /><br />
 <label htmlFor="other">Gender:  </label>
 <label htmlFor="male" >Male</label>
     <input type="radio" id='male' value='male' checked={gender==='male'} onChange={(e)=>{setGender(e.target.value)}} />
     <label htmlFor="female">Female</label>
     <input type="radio" id='female' value='female' checked={gender==='female'} onChange={(e)=>{setGender(e.target.value)}}/>
     <label htmlFor="other">Other</label>
     <input type="radio" id='other' value='other' checked={gender==='other'} onChange={(e)=>{setGender(e.target.value)}}/>
     <br />

         <br></br>
        <button type='submit'>Submit</button>
    </form>
  )
}

export default Form2

//sabai ma e.target.value tara boxes word aayo ki e.target.checked
//all uses value but boxes uses checked
// all usese e.target.value but checkbox uses checked
