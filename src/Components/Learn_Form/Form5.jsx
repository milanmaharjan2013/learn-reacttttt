// make  form with field name,age,email,gender


import React, { useState } from 'react'

const Form5 = () => {
    let [name, setName]=useState("")
    let [age, setAge]=useState("")
    let [email, setEmail]=useState("")
    let [gender, setGender]=useState("")
  return (
    <>
    <form action="" onSubmit={()=>{
        let obj ={
            name,
            age,
            email,
            gender
        }
        console.log(obj)
    }}>
        <label htmlFor="name">Name: </label>
        <input type="text" id='name' placeholder='Enter Your Name' value={name} onChange={(e)=>{
            setName(e.target.value);
        }}/> <br />
        <label htmlFor="age">Age: </label>
        <input type="number" id='age' placeholder='Enter Your Age' value={age} onChange={(e)=>{
            setAge(e.target.value);
        }} /> <br />
        <label htmlFor="email">Email: </label>
        <input type="email" name="" id="email" placeholder='Enter Your Email' value={email} onChange={(e)=>{
            setEmail(e.target.value);
        }} /> <br />
        <label htmlFor="gender">Gender: </label>
        <label htmlFor="male" >Male</label>
        <input type="radio" id='male' value="male" checked={gender==="male"} onChange={(e)=>{setGender(e.target.value)}}/>
        <label htmlFor="female">Female</label>
        <input type="radio" id='female' value="female" checked={gender==="female"} onChange={(e)=>{setGender(e.target.value)}}/>
        <label htmlFor="others">Other</label>
        <input type="radio" id=' others' value="others" checked={gender==="others"} onChange={(e)=>{setGender(e.target.value)}} /> <br />
        <button type='submit'>Submit</button>
    </form>
    </>
  )
}

export default Form5