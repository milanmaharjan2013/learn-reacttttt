import React, { useState } from 'react'
import A1 from './A1'

const A2 = () => {
    let [HideandShow, setHideandShow] =useState(true)
  return (
    <div>
        {
            // HideandShow?<A1></A1>:null  //using ternary operator
            HideandShow && <A1> </A1> // show A1 component if HideandShow is true //using AND operator
            // when a component is deleted or removed nothing will be execute
            //but if useEffect is used in that component and return is used in useEffect then return part will execute and other will not execute
        }
        <button
        onClick={()=>{
            setHideandShow(!HideandShow)
        }}
        >Hide the component</button>
    </div>
  )
}

export default A2