import React from 'react'

const A1 = () => {
  return (
    <div>
        <p>This component need to be hide and shown from button of other component</p>
    </div>
  )
}

export default A1