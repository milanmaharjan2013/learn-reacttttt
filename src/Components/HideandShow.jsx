import React, { useState } from 'react'

const HideandShow = () => {
    let [show,setShow]=useState(true)
  return (
    <div>
        <br />
        <hr />
        {
            show?<p>Error Has Occured</p>:null
        }
        <button onClick={()=>{
            setShow(false)
        }}>Hide</button> {''} 
        <button onClick={()=>{
            setShow(true)
        }}
        >Show</button>
        <hr />
    </div>
  )
}

export default HideandShow