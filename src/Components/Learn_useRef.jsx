import React, { useRef } from 'react'

const Learn_useRef = () => {
    // let inputRef1 = useRef()
    let inputRef2  =useRef()
    // let obj={
    //     name:'milan '
    // }
  return (
    <div>

        {/* <p ref={inputRef1}>paragraph 1</p>
        <p>paragraph 2</p>
        <button onClick={()=>{
            inputRef1.current.style.backgroundColor ="red"
        }}>Click to change bg color</button> */}
        <input type="text" ref = {inputRef2} />
        <button onClick={()=>{
            inputRef2.current.focus()
        }}>focus input</button>
        {/* {
            obj
        } */}   
        {/* div bhitra object call garna mildaina */}
    </div>
  )
}

export default Learn_useRef