import React, { useEffect, useState } from 'react'

const LearnuseEffect = () => {

    let [count, setCount]=useState(0)

    useEffect(()=>{
        console.log("hello 3")
        return ()=>{
            console.log("i am useEffect return 2 ")
        }
    },[count])
 
    console.log("i am seconddddddddddddddddddddddddddd 1")
  return (
    <div>LearnuseEffect
        <br />
        {count} <br />
         <button
         onClick={()=>{
            setCount(count+1)
         }}
         >
            Change
         </button>
         
    </div>

  )
}

export default LearnuseEffect

//return lekhyo vane function return garna parcha use effect ma
//in 1st render it doesn't execute 
//from second render it will run 
//useEffect bhitra ko return ko function lai clean up function vanxa 
// component did mount => suru ko render
// component did update => from second render to .... if dependencies are changeable
// component did unmount => delete , return ko code execute hunxa

// use effect ma tinta case hunxa [] navako [count] vako ani [] matrai vako
//[] yo vayena vane harek choti execute hunxa
// [] yo matrai vayo vane ek choti execute gar tespaxi execute nagar
//[count] => yo bhaneko count ko value change vayo vane matrai execute hunxa