import React, { useState } from 'react'

const Learn_useState = () => {
    //declaring variable using useState
    let [count1, setCount1] = useState(10)  
    console.log("Hello world!")
    let [count2, setCount2] = useState(0)
    console.log("hello")
  return (
    <div>
        {/* calling variable of useState */}
        {
            count1
            
        }
        <br />
        <button 
        onClick={()=>{
            // setCount1(count1+1)
            //setCount1(0)
            //setCount1(1)
            
        }}>Change the count1</button> <br />
        {
            count2
            
        }
        <br />
        <button 
        onClick={()=>{
            // setCount2(count2+1)
            // setCount2(()=>{s
            //     return count2+1
            // })
            setCount2((pre)=>{
                return pre+1
            })
            setCount2((pre)=>{
                return pre+1
            })
            setCount2((pre)=>{
                return pre+1
            })
            //pre vane si memory queue ma heryo tei vayera count sidhai 3 huna janxa aru ma 1 hunxa
            
        }}>Change the count2</button>
    </div>
  )
}

export default Learn_useState


// Rendering => code re execute hunu 
//state variable change huda sabai code re execute hunxa
//Component will be re-render if state variable changes, if state variable is not changes or affected then component will not re-render