import React, { useEffect, useState } from 'react'

const UseEffect = () => {
    let [count1,setCount1]  = useState(0)
    useEffect(()=>{
        console.log("I am first")
        console.log("Hello")
        console.log("World")
    return ()=>{
        console.log("i am second")
        console.log("this is return")
    }

    },[count1]
    )
    console.log("I am third")
  return (
    <div>UseEffect
        <br />
        {count1}
        <br />
        <button onClick={()=>{
            setCount1(count1+1)
        }}>change</button>

    </div>
  )
}

export default UseEffect