import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
import reportWebVitals from './reportWebVitals';
import Nitan from './Nitan';
import './Learn.css'
import { BrowserRouter } from 'react-router-dom';
const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  // <React.StrictMode>
 <BrowserRouter>
  <Nitan ></Nitan>
  </BrowserRouter>

  // {/* </React.StrictMode> */}
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();


{/* <p>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Quia, fugit laborum ipsa necessitatibus quasi maxime pariatur beatae? Similique quibusdam, ad expedita quia iure corrupti aspernatur cum blanditiis, id quo beatae?</p>
<h1>this is heading 1</h1>
<h2>this is heading 2</h2>
<h3>this is heading 3</h3>
<h4>this is heading 4</h4>
<h5>this is heading 5</h5>
<h6>this is heading 6</h6>
<img src="./logo192.png" alt="" height="100px" width="100px"></img> 
image sadhai public bhitra halne
 {/*in image case  . means public folder but in other case  .  means aafno folder  */}
{/* <a href="https://undraw.co/search" target='_blank'>click to undraw</a>
<div>hello <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Rem pariatur, asperiores praesentium quos blanditiis, aperiam aspernatur ipsum dignissimos amet quam fugit cumque ab! Eaque corrupti, dolorum exercitationem magni possimus placeat?</p></div>
Name<input type="text" name="" id="" ></input><br />
Address<input type="text" name="" id="" ></input> <br />
Email<input type="text" name="" id="" ></input> <br /> 

component sadhai capital letter huna paryo, sadhai return huna paryo ani return le j tag pani return garxa
component call garda <></> tag jasari call garna paryo
return ma euta tag matrai return hunxa tara div bhitra jati ota pani tag rakhna milyo
component ma parameters bhandaina props vanxa i.e. propoerties
ctrl+f garne thau thau ma naam haru change garna paryo vane
jsx=> javascript XML=> js with html is jsx
props ma string bahek aru lekhna paryo vane {} curly braces bhitra lekhna 
*/} 

// let num = [1,2,1,5,3,4,10,2]
// let sortArr = num.sort((a,b)=>{
//   return a-b
// })
// console.log(sortArr)
// OR

// let num = [1,2,1,5,]
// let sortArr = num.sort((a,b)=>
// a-b
// )
// console.log(sortArr)

//string sort
// let name =["ram",'shyam','hari','gita','ramesh']
// let sortNameLength = name.sort((a,b)=>{
//   return a.length - b.length
// })
// console.log(sortNameLength)

// let data=[{
//   id: 1,
//   title: "Product 1",
//   category: "electronics",
//   price: 5000,
//   description: "This is description and Product 1",
//   discount: {
//     type: "other",
//   },
// },
// {
//   id: 2,
//   title: "Product 2",
//   category: "cloths",
//   price: 2000,
//   description: "This is description and Product 2",
//   discount: {
//     type: "other",
//   },
// },
// {
//   id: 3,
//   title: "Product 3",
//   category: "electronics",
//   price: 3000,
//   description: "This is description and Product 3",
//   discount: {
//     type: "other",
//   },
// },
// ];


// let ids = data.sort((a,b)=>{
//   return a.id- b.id
// })
// console.log(ids)
// let cate = data.sort((a,b)=>{
//   return a.category.length - b.category.length
// })
// console.log(cate)

// let prices = data.sort((a,b)=>{
//   return a.price - b.price
// })
// console.log(prices)

